<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210116185023 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE csp (id INT AUTO_INCREMENT NOT NULL, domain_id INT DEFAULT NULL, document_uri VARCHAR(255) NOT NULL, referrer VARCHAR(255) NOT NULL, violated_directive VARCHAR(255) NOT NULL, effective_directive VARCHAR(255) NOT NULL, original_policy VARCHAR(255) NOT NULL, disposition VARCHAR(255) NOT NULL, blocked_uri VARCHAR(255) NOT NULL, status_code INT NOT NULL, script_sample VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, INDEX IDX_E1C7F762115F0EE5 (domain_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE csp ADD CONSTRAINT FK_E1C7F762115F0EE5 FOREIGN KEY (domain_id) REFERENCES domain (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE csp DROP FOREIGN KEY FK_E1C7F762115F0EE5');
        $this->addSql('DROP TABLE csp');
    }
}
