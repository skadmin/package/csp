<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210315165540 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'csp.overview', 'hash' => '1383bd30d21a8f31e07b32763383c619', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CSP', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.csp.title', 'hash' => 'bfcff6fcf5daf787dead9b46b6fa5bc8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CSP', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.csp.description', 'hash' => '15d202db2c2db225f16e4b4b6b36e030', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje spravovat content security policy', 'plural1' => '', 'plural2' => ''],
            ['original' => 'csp.overview.title', 'hash' => '5e9af2ec88de25ef36633219c65d3b26', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CSP|Přehled', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.domain', 'hash' => 'cfec571fd75ab4b23a38cd5d1ca511c8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Doména', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.counter', 'hash' => 'bc570542ef14d5aa2773c59e98a5a85a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Počet', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.blocked-uri', 'hash' => 'd5b6de5136b499839a94018d0fc7310c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zablokované uri', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.effective-directive', 'hash' => 'ecd9c86845e44a4a3c15372be84c42eb', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Direktiva', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.last-update-at', 'hash' => '63fd7d2d417a85ec5dbd3ffd0427d83a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Poslední záznam', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.detail.create-at', 'hash' => '9883f843b29494d96daaf5cccbe01986', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Vytvořeno', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.detail.domain', 'hash' => '1b677faa136e0b4bb701e809af80cd7d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Doména', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.detail.blocked-uri', 'hash' => '16e6415aab5fb3fa6ce9e7f369489b24', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zablokované uri', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.detail.sources', 'hash' => '78b0f13671a63b49826cef277f7d9c5e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zdroje', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.detail.original-policy', 'hash' => '02296a8e7d631b16414578f188dee9a9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'CSP', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.csp.overview.detail.blocked-uri-queries', 'hash' => 'd415de683bd9a899632293cdb93d1b3c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Zablokované uri včetně parametrů', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
