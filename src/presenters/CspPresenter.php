<?php

declare(strict_types=1);

namespace Skadmin\Csp\Presenter;

use Nette\Application\UI\Presenter;
use Nette\DI\Attributes\Inject;
use Nette\Http\Request;
use Skadmin\Csp\Doctrine\Csp\CspFacade;

use function json_decode;

class CspPresenter extends Presenter
{
    #[Inject]
    public Request $httpRequest;

    #[Inject]
    public CspFacade $facadeCsp;

    public function actionReceive(): void
    {
        $rawBody = $this->httpRequest->getRawBody();
        //$rawBody = '{"csp-report":{"document-uri":"http://control.skadmin.loc/authenticator/?bl=tsjaw","referrer":"","violated-directive":"img-src","effective-directive":"img-src","original-policy":"report-uri https://www.control.skadmin.cz/service/csp/receive; default-src \'self\'; script-src \'self\' \'unsafe-inline\' https://cdn.tiny.cloud; frame-src \'self\'; style-src \'self\' \'unsafe-inline\' https://cdn.tiny.cloud https://fonts.googleapis.com; font-src \'self\' data: https://cdn.tiny.cloud https://fonts.gstatic.com; img-src data: blob: https://sp.tinymce.com; connect-src \'self\' https://rainmaker.tiny.cloud;","disposition":"enforce","blocked-uri":"http://control.skadmin.loc/favicon.ico","status-code":200,"script-sample":""}}';

        if ($rawBody === null) {
            $this->sendJson(['result' => 'error']);
        }

        $cspReport = json_decode($rawBody, true)['csp-report'];
        $this->facadeCsp->create(
            $cspReport['document-uri'],
            $cspReport['referrer'],
            $cspReport['violated-directive'],
            $cspReport['effective-directive'],
            $cspReport['original-policy'],
            $cspReport['disposition'] ?? '',
            $cspReport['blocked-uri'] ?? '',
            $cspReport['status-code'] ?? '',
            $cspReport['script-sample'] ?? ''
        );

        $this->sendJson(['result' => 'ok']);
    }
}
