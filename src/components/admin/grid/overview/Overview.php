<?php

declare(strict_types=1);

namespace Skadmin\Csp\Components\Admin;

use App\Model\System\APackageControl;
use Nette\ComponentModel\IContainer;
use Nette\Security\User;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Skadmin\Csp\BaseControl;
use Skadmin\Csp\Doctrine\Csp\Csp;
use Skadmin\Csp\Doctrine\Csp\CspFacade;
use Skadmin\Role\Doctrine\Role\Privilege;
use Skadmin\Translator\Translator;
use SkadminUtils\GridControls\UI\GridControl;
use SkadminUtils\GridControls\UI\GridDoctrine;

class Overview extends GridControl
{
    use APackageControl;

    private CspFacade $facade;

    public function __construct(CspFacade $facade, Translator $translator, User $user)
    {
        parent::__construct($translator, $user);
        $this->facade = $facade;
    }

    /**
     * @return static
     */
    public function setParent(?IContainer $parent, ?string $name = null): static
    {
        parent::setParent($parent, $name);

        if (! $this->isAllowed(BaseControl::RESOURCE, Privilege::READ)) {
            $this->getParent()->redirect(':Admin:Homepage:accessDenied');
        }

        return $this;
    }

    public function render(): void
    {
        $template = $this->getComponentTemplate();
        $template->setTranslator($this->translator);
        $template->setFile(__DIR__ . '/overview.latte');
        $template->render();
    }

    public function getTitle(): string
    {
        return 'csp.overview.title';
    }

    protected function createComponentGrid(string $name): GridDoctrine
    {
        $grid = new GridDoctrine($this->getPresenter());

        // DEFAULT
        $grid->setPrimaryKey('id');
        $grid->setDataSource($this->facade->getModel()
            ->orderBy('a.lastUpdateAt', 'DESC'));

        // COLUMNS
        $grid->addColumnText('domain', 'grid.csp.overview.domain')
            ->setRenderer(static function (Csp $csp): Html {
                $domain = $csp->getDomain();

                $website = Html::el('a', [
                    'href'   => $domain->getWebsite(),
                    'target' => '_blank',
                    'class'  => 'text-primary text-nowrap',
                ])->setText($domain->getWebsite());

                if ($domain->getWebsite() === $domain->getName()) {
                    return $website;
                }

                $html = new Html();

                $name = Html::el('div', ['class' => 'text-primary text-nowrap'])->setText($domain->getName());

                $code = Html::el('code', ['class' => 'text-muted small'])->setHtml($website);

                $html->setHtml($name)
                    ->addHtml($code);

                return $html;
            });

        $grid->addColumnNumber('counter', 'grid.csp.overview.counter')
            ->setAlign('center');
        $grid->addColumnText('blockedUri', 'grid.csp.overview.blocked-uri')
            ->setRenderer(static function (Csp $csp): string {
                return Strings::truncate($csp->getBlockedUri(), 50);
            });
        $grid->addColumnText('effectiveDirective', 'grid.csp.overview.effective-directive');
        $grid->addColumnDateTime('lastUpdateAt', 'grid.csp.overview.last-update-at')
            ->setFormat('d.m.Y H:i:s');

        // FILTER
        //$grid->addFilterText('name', 'grid.csp.overview.name');

        // DETAIL
        $grid->setItemsDetail(__DIR__ . '/detail.latte');

        // ACTION
        // TOOLBAR
        // ALLOW

        return $grid;
    }
}
