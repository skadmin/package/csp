<?php

declare(strict_types=1);

namespace Skadmin\Csp\Components\Admin;

interface IOverviewFactory
{
    public function create(): Overview;
}
