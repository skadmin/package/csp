<?php

declare(strict_types=1);

namespace Skadmin\Csp\Doctrine\Csp;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Skadmin\Domain\Doctrine\Domain\Domain;
use SkadminUtils\DoctrineTraits\Entity;

use function in_array;

#[ORM\Entity]
#[ORM\HasLifecycleCallbacks]
class Csp
{
    use Entity\Id;
    use Entity\IsActive;
    use Entity\Created;
    use Entity\LastUpdateDate;
    use Entity\Counter;

    /** @var array<int, string> */
    #[ORM\Column(type: Types::ARRAY)]
    protected array $sources = [];

    #[ORM\Column]
    protected string $violatedDirective;

    #[ORM\Column]
    protected string $effectiveDirective;

    #[ORM\Column(type: Types::TEXT)]
    protected string $originalPolicy;

    #[ORM\Column]
    protected string $disposition;

    #[ORM\Column]
    protected string $blockedUri;

    /** @var array<int, string> */
    #[ORM\Column(type: Types::ARRAY)]
    protected array $blockedUriQueries = [];

    #[ORM\Column]
    protected int $statusCode;

    #[ORM\Column]
    protected string $scriptSample;

    #[ORM\ManyToOne(targetEntity: Domain::class, cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private Domain $domain;

    public function create(Domain $domain, string $source, string $violatedDirective, string $effectiveDirective, string $originalPolicy, string $disposition, string $blockedUri, string $blockedUriQuery, int $statusCode, string $scriptSample): void
    {
        $this->domain              = $domain;
        $this->sources[]           = $source;
        $this->violatedDirective   = $violatedDirective;
        $this->effectiveDirective  = $effectiveDirective;
        $this->originalPolicy      = $originalPolicy;
        $this->disposition         = $disposition;
        $this->blockedUri          = $blockedUri;
        $this->blockedUriQueries[] = $blockedUriQuery;
        $this->statusCode          = $statusCode;
        $this->scriptSample        = $scriptSample;
    }

    public function addSource(string $source): void
    {
        $this->counter++;

        if (in_array($source, $this->sources)) {
            return;
        }

        $this->sources[] = $source;
    }

    public function addBlockedUriQuery(string $blockedUriQuery): void
    {
        if (in_array($blockedUriQuery, $this->blockedUriQueries)) {
            return;
        }

        $this->blockedUriQueries[] = $blockedUriQuery;
    }

    /**
     * @return array<int, array<string, string>>
     */
    public function getSources(): array
    {
        return $this->sources;
    }

    public function getViolatedDirective(): string
    {
        return $this->violatedDirective;
    }

    public function getEffectiveDirective(): string
    {
        return $this->effectiveDirective;
    }

    public function getOriginalPolicy(): string
    {
        return $this->originalPolicy;
    }

    public function getDisposition(): string
    {
        return $this->disposition;
    }

    public function getBlockedUri(): string
    {
        return $this->blockedUri;
    }

    /**
     * @return array<int, string>
     */
    public function getBlockedUriQueries(): array
    {
        return $this->blockedUriQueries;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getScriptSample(): string
    {
        return $this->scriptSample;
    }

    public function getDomain(): Domain
    {
        return $this->domain;
    }
}
