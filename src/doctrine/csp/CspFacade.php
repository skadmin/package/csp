<?php

declare(strict_types=1);

namespace Skadmin\Csp\Doctrine\Csp;

use Nette\Http\UrlScript;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Domain\Doctrine\Domain\Domain;
use Skadmin\Domain\Doctrine\Domain\DomainFacade;
use SkadminUtils\DoctrineTraits\Facade;

final class CspFacade extends Facade
{
    private DomainFacade $facadeDomain;

    public function __construct(EntityManagerDecorator $em, DomainFacade $facadeDomain)
    {
        parent::__construct($em);
        $this->facadeDomain = $facadeDomain;

        $this->table = Csp::class;
    }

    public function create(string $documentUri, string $referrer, string $violatedDirective, string $effectiveDirective, string $originalPolicy, string $disposition, string $blockedUri, int $statusCode, string $scriptSample): ?Csp
    {
        $url    = new UrlScript($documentUri);
        $domain = $this->facadeDomain->findByWebsiteOrCreate($url->getHost());

        if (! $domain->isActive()) {
            return null;
        }

        $blockedUriParsed = new UrlScript($blockedUri);
        $blockedUri       = $blockedUriParsed->getBaseUrl();
        $blockedUriQuery  = $blockedUriParsed->getQuery();

        $csp = $this->findByDomainAndBlockedUri($domain, $blockedUri);
        if ($csp instanceof Csp) {
            $csp->addSource($documentUri);
            $csp->addBlockedUriQuery($blockedUriQuery);
        } else {
            $csp = $this->get();
            $csp->create($domain, $documentUri, $violatedDirective, $effectiveDirective, $originalPolicy, $disposition, $blockedUri, $blockedUriQuery, $statusCode, $scriptSample);
        }

        $this->em->persist($csp);
        $this->em->flush();

        return $csp;
    }

    public function get(?int $id = null): Csp
    {
        if ($id === null) {
            return new Csp();
        }

        $csp = parent::get($id);

        if ($csp === null) {
            return new Csp();
        }

        return $csp;
    }

    public function findByDomainAndBlockedUri(Domain $domain, string $blockedUri): ?Csp
    {
        $criteria = [
            'domain'     => $domain,
            'blockedUri' => $blockedUri,
        ];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }
}
